# Battle TODO

## SYSTEM OVERVIEW

- 턴제 전투
-1TURN = 지정 시간의 경과 (현재 세팅 : 10초 / NOT REALTIME)

- Character가 행동을 결정
- 각 행동은 시전 시간 or 행동 시간 (Activation Time)
- 캐릭터의 스테이터스에 따라 각 행동의 시전시간 증가/감소 

## BATTLE CONSEQUENCES

1. 전투는 턴제 방식으로 진행된다. 
2. 턴의 진행 순서는 다음과 같다.
전투 세팅 -> 플레이어 파티 행동 결정 -> 적 캐릭터 행동 결정(AI) -> 행동 수행 -> 전투불능 캐릭터 확인 -> 플레이어 승리/패배 확인 -> 전투 종료
3. 전투는 플레이어가 승리/패배 시점까지 반복된다. 반복되는 구간은 "플레이어 파티 행동 결정 -> 적 캐릭터 행동 결정(AI) -> 행동 수행 -> 전투불능 캐릭터 확인 -> 플레이어 승리/패배 확인" 이다.
3. 통상적인 턴제 전투와 달리 이 시스템은 플레이어와 적의 행동이 각 행동의 castTime 수치에 따라 행동한다. 
4. 즉, 적과 플레이어가 동시에 행동할 수 있다는 것이다. 
5. 한 턴은 n초의 시간을 가지고 있고 n은 변경할 수 있다. 기본은 10초로 한다. 
6. 턴의 진행은 10초의 시간 동안 캐릭터들이 결정한 행동이 시간 내 수행되는 것이다. 
7. 예를 들어 기본 공격의 castTime이 5초라면 그 공격은 턴이 시작되고 5초 후 적용된다. 
8. 그리고 castTime이 5초, 턴 진행 시간이 10초 이기에 해당 캐릭터는 원한다면 2회 공격이 가능하다. 
9. 즉 턴 시간 안이라면 공격 횟수의 제한은 없다. 
10. 1회 이상의 공격을 하는 경우 행동의 순서는 먼저 선택한 행동부터 수행된다.
11. 행동 수행 중 체력이 0 이하로 된 캐릭터는 행동 불능 상태가 된다. 해당 캐릭터는 수행하기 전 행동이 전부 취소된다. 
12. 이후 '전투불능 캐릭터 확인' 시점에서 체력이 0이하라면 그 캐릭터는 전투 불능 상태가 된다.
13. 플레이어 승리 조건은 적 캐릭터를 전부 전투불능 상태로 만드는 것이다.
14. 플레이어 패배 조건은 플레이어 파티 중 isMainCharacter 속성이 True인 캐릭터가 전투 불능 상태인 경우 패배한다. 
15. isMainCharacter가 false인 다른 플레이어 캐릭터가  전투 불능 상태가 되더라도 패배하지 않는다. 

1. Battles will be turn-based 
2. The sequence of turns is as follows;
Set up combat -> Decide player party actions -> Decide enemy character actions (AI) -> Execute actions -> Check incapacitated characters -> Check player win/loss -> End combat.
3. The battle is repeated until the player win or lose. 
The loop sequence is "Player Party Action Decision -> Enemy Character Action Decision (AI) -> Execute actions -> Check incapacitated characters -> Check player win/loss".
4. Unlike normal turn-based combat, this system allows player and enemy actions to be performed based on the 'castTime' value of each action. 
5. Means that Enemies and players can act simultaneously. 
6. A turn has a duration of 'n' seconds, where 'n' can be changed. The default is 10 seconds. 
7. The progression of a turn is that the actions decided by the characters during the 10 seconds are performed in time. 
8. For example, if a basic attack has a castTime of 5 seconds, the attack is applied 5 seconds after the turn begins. 
9. Since the castTime is 5 seconds and the turn duration remains 5 seconds extra, the character can make more actions if they want. 
10. There is no limitation of number of attacks, as long as they are within the turn time. 
11. If you make more than one attack, the order of actions is based on queue system.
12. If a character reaches 0 health or less while performing an action, they are temporally incapacitated. All of the character's actions are canceled. 
13. If a character has less than 0 hit points at the time of the subsequent "Check incapacitated characters", the character is incapacitated.
14. The player victory condition is to incapacitate all enemy characters.
15. the player loses if any character in the player's party whose isMainCharacter property is True is incapacitated. 
16. Player do not lose if another player character with isMainCharacter false is incapacitated. 

1. 메뉴나 사용자 인터페이스가 존재한다. 

2. 특정 행동 패턴이 있으면 좋겠지만, 우선은 사용 가능한 작업에서 단순 임의 선택을 하는 방법으로 결정한다. 나중에 여유가 된다면 Knapsack problem과 같은 방법으로 행동을 결정하는 방법을 생각해 보겠다.

3. 캐릭터의 다른 능력치가 castTime에 영향을 주게 될 것이다. 정확하게는 캐릭터의 level, magic이 castTime에 영향을 준다. 즉, 준비해야 할 것은 캐릭터의 level, magic 수치에 비례해서 skill의 castTime을 줄이는 기능이다. 

4. "Check incapacitated characters" 단계에서 무력화가 확정되었다면 부활할 수 있는 방법은 없다.

5. castTime이 같다면 동시에 행동한다. 동시 행동의 구현은 asysc await로 구현할 계획이다. 


1. Yes. a menu or user interface will exists. 

2. It would be nice to have a specific behavioral pattern. But for now, we'll settle for a simple random selection from the available actions. Later, when we have time, we'll think about determining behavior in the similar way as the 'Knapsack problem'.

3. The character's other stats will affect the 'castTime'. To be precise, the character's 'level' and 'magic' will affect the 'castTime'. So what we need to prepare is a function that reduces the 'castTime' of a 'skill' in proportion to the character's 'level' and 'magic'. 

4. During the "Check incapacitated characters" step confirms character's incapacitation, there is no way to revive them.

5. If the 'castTime' is the same, they will act at the same time. The implementation of simultaneous actions is planned to be implemented with asysc await. 

Create a new CombatManager class responsible for managing the turn-based combat system. This class should have methods for setting up the combat, managing turns, and handling win/loss conditions.

Implement a menu or user interface for the player to select actions for their party members.

For enemy AI, create a simple function to randomly select an action from the available actions. This function can be expanded later to incorporate more complex decision-making.

Implement a function that calculates the actual castTime of a skill based on the character's level and magic. This function should be called whenever a character selects an action.

Manage the execution of actions using a priority queue or a sorted list, with the castTime as the key. When the castTime is the same for two or more characters, process their actions simultaneously using async/await.

At the end of each turn, perform checks for incapacitated characters and update their status accordingly. Also, check for win/loss conditions and handle them accordingly.

Loop through the turn sequence (player party actions -> enemy actions -> execute actions -> check incapacitated characters -> check win/loss) until the battle ends with either a win or loss.

This is a high-level structure for the combat system, and specific implementation details may vary. You can use this outline as a starting point and adapt it to your needs. If you need help with any specific part of the implementation or have further questions, please let me know.



턴 기반 전투 시스템 관리를 담당하는 새로운 CombatManager 클래스를 만듭니다. 이 클래스에는 전투 설정, 턴 관리 및 승/패 조건 처리를 위한 메서드가 있어야 합니다.
플레이어가 파티원을 위한 작업을 선택할 수 있도록 메뉴 또는 사용자 인터페이스를 구현합니다.
적 AI에 대해 사용 가능한 동작에서 임의로 동작을 선택하는 간단한 기능을 만듭니다. 이 기능은 나중에 확장하여 보다 복잡한 의사 결정을 통합할 수 있습니다.
캐릭터의 레벨과 마법에 따라 스킬의 실제 'castTime'을 계산하는 기능을 구현합니다. 이 함수는 캐릭터가 행동을 선택할 때마다 호출되어야 합니다.
'castTime'을 키로 사용하여 우선순위 대기열 또는 정렬된 목록을 사용하여 작업 실행을 관리합니다. 두 개 이상의 캐릭터에 대해 'castTime'이 동일한 경우 async/await를 사용하여 동시에 작업을 처리합니다.
각 차례가 끝날 때마다 무력화된 캐릭터를 확인하고 그에 따라 상태를 업데이트합니다. 또한 승패 조건을 확인하고 그에 따라 처리하십시오.
전투가 승패로 끝날 때까지 차례 순서(플레이어 파티 행동 -> 적 행동 -> 행동 실행 -> 무력화 캐릭터 확인 -> 승/패 확인)를 반복합니다.
