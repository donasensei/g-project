using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    private void Awake() // Singleton
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static GameManager Instance
    {
        get { return instance; }
    }

    // 게임 프레임을 60으로 제한하는 함수
    private void LimitFrameRate()
    {
        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        Debug.Log(Application.persistentDataPath);
        // 프레임 제한
        LimitFrameRate();
    }
}

