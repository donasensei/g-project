using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Characters/New Character")]
public class Character : ScriptableObject
{
    [SerializeField] private string characterName;
    [TextArea][SerializeField] private string description;


    [SerializeField] private Sprite sprite;
    [SerializeField] private int level;
    [SerializeField] private int maxHP;
    [SerializeField] private int maxMP;

    [SerializeField] private int attack;
    [SerializeField] private int maAttack;

    [SerializeField] private int strength;
    [SerializeField] private int magic;
    [SerializeField] private int vitality;

    [SerializeField] private int currentHP;
    [SerializeField] private int currentMP;

    [SerializeField] private bool isMainCharacter;

    [SerializeField] private List<Skill> skills = new List<Skill>();

    public string CharacterName { get { return characterName; } set { characterName = value; } }
    public string Description { get { return description; }  set { description = value; } }
    public Sprite CharacterSprite { get { return sprite; } set {  sprite = value; } } 

    public int Level
    {
        get { return level; }
        set
        {
            level = value;
            RecalculateStats();
        }
    }
    public int MaxHP { get { return maxHP; } }
    public int MaxMP { get { return maxMP; } }
    public int Attack { get { return attack; } }
    public int MaAttack { get { return maAttack; } }
    public int Strength
    {
        get { return strength; }
        set
        {
            strength = Mathf.Clamp(value, 1, 99);
            RecalculateStats();
        }
    }
    public int Magic
    {
        get { return magic; }
        set
        {
            magic = Mathf.Clamp(value, 1, 99);
            RecalculateStats();
        }
    }
    public int Vitality
    {
        get { return vitality; }
        set
        {
            vitality = Mathf.Clamp(value, 1, 99);
            RecalculateStats();
        }
    }
    public int CurrentHP
    {
        get { return currentHP; }
        set
        {
            currentHP = Mathf.Clamp(value, 0, MaxHP);
        }
    }
    public int CurrentMP
    {
        get { return currentMP; }
        set
        {
            currentMP = Mathf.Clamp(value, 0, MaxMP);
        }
    }
    public bool IsMainCharacter
    {
        get { return isMainCharacter; }
        set { isMainCharacter = value; }
    }
    private void OnEnable()
    {
        RecalculateStats();
        currentHP = MaxHP;
        currentMP = MaxMP;
    }

    public List<Skill> Skills { get { return skills; } }

    private void RecalculateStats()
    {
        maxHP = (vitality * 6) + (level * 6);
        maxMP = (magic * 3) + (level * 3);
        attack = Mathf.FloorToInt((level + strength) * 32 * 0.07f);
        maAttack = Mathf.FloorToInt((level + magic) * 16 * 0.07f);
    }
}