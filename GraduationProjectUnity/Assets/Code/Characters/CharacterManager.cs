using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    [SerializeField] private Character character;

    void Start()
    {
        if (character == null)
        {
            Debug.LogError("Character is NULL!");
        }
    }
    private void Update()
    {

    }
    public void LogData()
    {
        // Output character's properties
        Debug.Log("Character Name: " + character.CharacterName);
        Debug.Log("Description: " + character.Description);
        Debug.Log("Level: " + character.Level);
        Debug.Log("Max HP: " + character.MaxHP);
        Debug.Log("Current HP: " + character.CurrentHP);
        Debug.Log("Max MP: " + character.MaxMP);
        Debug.Log("Current MP: " + character.CurrentMP);
        Debug.Log("Attack: " + character.Attack);
        Debug.Log("Magic Attack: " + character.MaAttack);
        Debug.Log("Strength: " + character.Strength);
        Debug.Log("Magic: " + character.Magic);
        Debug.Log("Vitality: " + character.Vitality);
        Debug.Log("Is Player Character: " + character.IsMainCharacter);
    }

    public void LevelUP()
    {
        character.Level++;
        character.Strength++;
        character.Magic++;
        character.Vitality++;
    }
}


