using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterHUD : MonoBehaviour
{
    [SerializeField] private Character playerCharacter;
    [SerializeField] private TextMeshProUGUI outputText;
    [SerializeField] private PartyManager partyManager;

    private void Update()
    {

    }

    public void ShowCharacterInfo(Character character)
    {
        outputText.text = $"Name: {character.CharacterName}\n" +
                                 $"Level: {character.Level}\n" +
                                 $"Strength: {character.Strength}\n" +
                                 $"Magic: {character.Magic}\n" +
                                 $"Vitality: {character.Vitality}\n" +
                                 $"Max HP: {character.MaxHP}\n" +
                                 $"Max MP: {character.MaxMP}\n" +
                                 $"Attack: {character.Attack}\n" +
                                 $"Magic Attack: {character.MaAttack}\n" +
                                 $"Description: {character.Description}";
    }
}
