using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Enemy ListList", menuName = "Characters/New Enemy List", order = 1)]
public class CharacterDataList : ScriptableObject
{
    public List<Character> characterDataList;
}
