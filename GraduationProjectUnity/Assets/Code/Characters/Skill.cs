using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSkill : ScriptableObject
{
    [SerializeField] string skillName;
    [SerializeField] int uniqueSkillNumber;

    [TextArea]
    [SerializeField] string description;
    [SerializeField] SkillType skillType;
    [SerializeField] SkillElement skillElement;

    [SerializeField] int rank;
    [SerializeField] int damage;
    [SerializeField] int duration;
    [SerializeField] int cost;
    [SerializeField] int castTime;

    // REF
    public string SkillName { get { return skillName; } }
    public int UniqueSkillNumber { get { return uniqueSkillNumber; } }
    public string Description { get { return description; } }
    public SkillType Type { get { return skillType; } }
    public SkillElement Element { get { return skillElement; } }
    public int Rank { get { return rank; } }
    public int Damage { get { return damage; } }
    public int Duration { get { return duration; } }
    public int Cost { get { return cost; } }
    public int CastTime { get { return castTime; } }
    public abstract void Activate(Character user, Character target);
}

public enum SkillType { None, Attack, Dot }
public enum SkillElement { None, Physical, Fire, Ice, Electric, Wind, Bless, Curse }

[CreateAssetMenu(fileName = "Skill", menuName = "RPG/New Skill")]
public class Skill : BaseSkill
{
    // Placeholder method for damage calculation
    public int CalculateDamage(Character user, Character target)
    {
        // Implement damage calculation logic here later
        return 0;
    }

    // Placeholder method for checking activation conditions
    public bool CanActivate(Character user, Character target)
    {
        // Implement activation conditions checking logic here later
        return true;
    }

    // Modify the existing Activate method to use the new methods
    public override void Activate(Character user, Character target)
    {
        if (CanActivate(user, target))
        {
            int damage = CalculateDamage(user, target);
            // Apply the damage or other effects to the target here
        }
    }
}

