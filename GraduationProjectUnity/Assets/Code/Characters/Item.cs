using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseItem : ScriptableObject
{
    [SerializeField] string itemName;

    [TextArea]
    [SerializeField] string description;
    [SerializeField] int value;
    [SerializeField] Sprite itemIcon;

    //REF
    public string ItemName { get { return itemName; } }
    public string Description { get { return description; } }
    public int Value { get { return value; } }
    public Sprite ItemIcon { get { return itemIcon; } }

    public abstract void Use(Character user);
}

[CreateAssetMenu(fileName = "Item", menuName = "RPG/New Item")]
public class Item : BaseItem
{
    public override void Use(Character user)
    {
        // Implement item usage logic here
    }
}