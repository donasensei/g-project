using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RandomCharacterGenerator : MonoBehaviour
{
    [SerializeField] private Character characterTemplate;
    [SerializeField] private string[] namesPool;
    [SerializeField] private Sprite[] spritesPool;
    [SerializeField] private int minLevel = 1;
    [SerializeField] private int maxLevel = 99;
    [SerializeField] private int minStatValue = 1;
    [SerializeField] private int maxStatValue = 99;

    // DEBUG
    [SerializeField] private PartyManager partyManager;
    [SerializeField] private Button generateButton;
    [SerializeField] private Button addButton;
    [SerializeField] private Button removeButton;

    [SerializeField] private TextMeshProUGUI characterInfoText;
    [SerializeField] private Image characterSpriteImage;

    private void Start()
    {
        generateButton.onClick.AddListener(() =>
        {
            Character randomCharacter = GenerateRandomCharacter();
            ShowCharacterInfo(randomCharacter);
        });

        addButton.onClick.AddListener(() =>
        {
            Character randomCharacter = GenerateRandomCharacter();
            ShowCharacterInfo(randomCharacter);
            bool added = partyManager.AddPartyMember(randomCharacter);
            if (added)
            {
                Debug.Log("Party member added");
            }
            else
            {
                Debug.Log("Party is full");
            }
        });

        removeButton.onClick.AddListener(() =>
        {
            RemoveRandomCharacter();
        });
    }

    public Character GenerateRandomCharacter()
    {
        Character newCharacter = Instantiate(characterTemplate);

        newCharacter.name = namesPool[Random.Range(0, namesPool.Length)];
        newCharacter.CharacterName = newCharacter.name;
        newCharacter.Description = "A randomly generated character.";
        newCharacter.CharacterSprite = spritesPool[Random.Range(0, spritesPool.Length)];
        newCharacter.Level = Random.Range(minLevel, maxLevel + 1);
        newCharacter.Strength = Random.Range(minStatValue, maxStatValue + 1);
        newCharacter.Magic = Random.Range(minStatValue, maxStatValue + 1);
        newCharacter.Vitality = Random.Range(minStatValue, maxStatValue + 1);
        newCharacter.IsMainCharacter = false;

        return newCharacter;
    }

    //DEBUG
    private void ShowCharacterInfo(Character character)
    {
        characterSpriteImage.sprite = character.CharacterSprite;

        characterInfoText.text = $"Name: {character.CharacterName}\n" +
                                 $"Level: {character.Level}\n" +
                                 $"Strength: {character.Strength}\n" +
                                 $"Magic: {character.Magic}\n" +
                                 $"Vitality: {character.Vitality}\n" +
                                 $"Max HP: {character.MaxHP}\n" +
                                 $"Max MP: {character.MaxMP}\n" +
                                 $"Attack: {character.Attack}\n" +
                                 $"Magic Attack: {character.MaAttack}\n" +
                                 $"Description: {character.Description}";
    }

    public void RemoveRandomCharacter()
    {
        if (partyManager.PartyMembers.Count > 1) // Ensure main character isn't removed
        {
            int randomIndex = Random.Range(1, partyManager.PartyMembers.Count); // Exclude main character (index 0)
            Character removedCharacter = partyManager.PartyMembers[randomIndex];
            partyManager.RemovePartyMember(removedCharacter);
            // Destroy(removedCharacter.gameObject); // Destroy removed character's GameObject
        }
        else
        {
            Debug.LogWarning("Cannot remove the main character from the party.");
        }
    }
}
