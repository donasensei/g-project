using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;

using UnityEngine;

public class PartyManager : MonoBehaviour
{
    public static PartyManager Instance;
    private SaveManager saveManager;

    [SerializeField] private Character characterPrefab;

    [SerializeField] private Character mainCharacter;
    [SerializeField] private List<Character> partyMembers;
    [SerializeField] private int maxPartySize = 4;

    private void Awake() // Singleton 
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        saveManager = new SaveManager();
    }

    private void Start()
    {
        LoadParty();
        // Ensure the main character is included in the party members list
        if (mainCharacter != null && !partyMembers.Any(pm => pm.IsMainCharacter))
        {
            AddPartyMember(mainCharacter);
        }
    }

    public Character MainCharacter
    {
        get { return mainCharacter; }
        set { mainCharacter = value; }
    }

    public List<Character> PartyMembers
    {
        get { return partyMembers; }
        set { partyMembers = value; }
    }

    public bool AddPartyMember(Character character)
    {
        if (partyMembers.Count < maxPartySize)
        {
            partyMembers.Add(character);
            return true;
        }
        return false;
    }

    public bool RemovePartyMember(Character character)
    {
        // Prevent removal of the main character
        if (character == mainCharacter)
        {
            Debug.LogWarning("Cannot remove the main character from the party.");
            return false;
        }

        return partyMembers.Remove(character);
    }

    public void SaveParty()
    {
        saveManager.SavePartyData(partyMembers);
    }

    public void LoadParty()
    {
        partyMembers = saveManager.LoadPartyData(characterPrefab);
    }
}
