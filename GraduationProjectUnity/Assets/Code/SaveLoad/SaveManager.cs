using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using UnityEngine;

public class SaveManager
{
    private string saveFilePath;
    public static SaveManager Instance { get; private set; }

    public SaveManager()
    {
        saveFilePath = Application.persistentDataPath + "/savegame.json";
    }

    public static void Initialize()
    {
        if (Instance == null)
        {
            Instance = new SaveManager();
        }
    }

    public void SavePartyData(List<Character> partyMembers)
    {
        PartyData partyData = new PartyData();
        partyData.partyMembersData = new List<CharacterData>();

        foreach (Character character in partyMembers)
        {
            partyData.partyMembersData.Add(new CharacterData(character));
        }

        GameData gameData = LoadGameData();
        gameData.partyData = partyData;
        SaveGameData(gameData);
        Debug.Log("Party saved.");
    }

    public List<Character> LoadPartyData(Character characterPrefab)
    {
        List<Character> partyMembers = new List<Character>();

        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            GameData gameData = JsonUtility.FromJson<GameData>(json);
            PartyData partyData = gameData.partyData;

            partyMembers.Clear();

            foreach (CharacterData characterData in partyData.partyMembersData)
            {
                Character newCharacter = Object.Instantiate(characterPrefab);
                newCharacter.CharacterName = characterData.characterName;
                newCharacter.Description = characterData.description;
                newCharacter.CharacterSprite = Resources.Load<Sprite>("Sprites/" + characterData.spritePath);
                newCharacter.Level = characterData.level;
                newCharacter.Strength = characterData.strength;
                newCharacter.Magic = characterData.magic;
                newCharacter.Vitality = characterData.vitality;
                newCharacter.IsMainCharacter = characterData.isMainCharacter;

                // Load skills for the character
                foreach (string skillPath in characterData.skillPaths)
                {
                    // Check if the character already has the skill
                    bool skillExists = false;
                    foreach (Skill existingSkill in newCharacter.Skills)
                    {
                        if (existingSkill.name == skillPath)
                        {
                            skillExists = true;
                            break;
                        }
                    }

                    // If the skill doesn't exist, load and add it
                    if (!skillExists)
                    {
                        Skill skill = Resources.Load<Skill>("Skills/" + skillPath);
                        if (skill != null)
                        {
                            newCharacter.Skills.Add(skill);
                        }
                        else
                        {
                            Debug.LogWarning($"Skill '{skillPath}' not found in Resources/Skills");
                        }
                    }
                }

                // Merge the default skills from the SampleCharacter ScriptableObject
                foreach (Skill defaultSkill in characterPrefab.Skills)
                {
                    // Check if the character already has the skill
                    bool skillExists = false;
                    foreach (Skill existingSkill in newCharacter.Skills)
                    {
                        if (existingSkill.name == defaultSkill.name)
                        {
                            skillExists = true;
                            break;
                        }
                    }

                    // If the skill doesn't exist, add it
                    if (!skillExists)
                    {
                        newCharacter.Skills.Add(defaultSkill);
                    }
                }
                partyMembers.Add(newCharacter);
            }
            Debug.Log("Party loaded.");
        }
        else
        {
            Debug.LogError("No save game file found.");
        }
        return partyMembers;
    }


    public void SaveStageInfo(StageInfo newStageInfo)
    {
        GameData gameData = LoadGameData();
        bool stageFound = false;

        for (int i = 0; i < gameData.stageInfoList.Count; i++)
        {
            if (gameData.stageInfoList[i].stageNumber == newStageInfo.stageNumber)
            {
                gameData.stageInfoList[i] = newStageInfo; // Update the existing stage info
                stageFound = true;
                break;
            }
        }

        if (!stageFound)
        {
            gameData.stageInfoList.Add(newStageInfo); // Add the new stage info to the list
        }

        SaveGameData(gameData);
        Debug.Log("Stage info saved.");
    }


    public List<StageInfo> LoadStageInfo()
    {
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            GameData gameData = JsonUtility.FromJson<GameData>(json);
            List<StageInfo> stageInfoList = gameData.stageInfoList;

            Debug.Log("Stage info loaded.");
            return stageInfoList;
        }
        else
        {
            Debug.LogError("No save game file found.");
            return new List<StageInfo>();
        }
    }

    private void SaveGameData(GameData gameData)
    {
        string json = JsonUtility.ToJson(gameData);
        File.WriteAllText(saveFilePath, json);
    }

    private GameData LoadGameData()
    {
        if (File.Exists(saveFilePath))
        {
            string json = File.ReadAllText(saveFilePath);
            GameData gameData = JsonUtility.FromJson<GameData>(json);
            return gameData;
        }
        else
        {
            return new GameData(new PartyData(), new List<StageInfo>());
        }
    }
}
