using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public PartyData partyData;
    public List<StageInfo> stageInfoList;

    public GameData(PartyData partyData, List<StageInfo> stageInfoList)
    {
        this.partyData = partyData;
        this.stageInfoList = stageInfoList;
    }
}

[System.Serializable]
public class PartyData
{
    public List<CharacterData> partyMembersData;
}

[System.Serializable]
public class CharacterData
{
    public string characterName;
    public string description;
    public string spritePath;
    public int level;
    public int strength;
    public int magic;
    public int vitality;
    public bool isMainCharacter;
    public List<string> skillPaths;

    public CharacterData(Character character)
    {
        this.characterName = character.CharacterName;
        this.description = character.Description;
        this.spritePath = character.CharacterSprite.name; // Assumes Sprite is in Resources folder
        this.level = character.Level;
        this.strength = character.Strength;
        this.magic = character.Magic;
        this.vitality = character.Vitality;
        this.isMainCharacter = character.IsMainCharacter;

        this.skillPaths = new List<string>();
        HashSet<string> addedSkillNames = new HashSet<string>();
        foreach (Skill skill in character.Skills)
        {
            // Check for duplicates
            if (!addedSkillNames.Contains(skill.name))
            {
                skillPaths.Add(skill.name); // Assumes Skill is in Resources folder
                addedSkillNames.Add(skill.name);
            }
        }
    }
}

[System.Serializable]
public class StageInfo
{
    public int stageNumber;
    public List<NodeData> nodes;
    public float visitedNodesPercentage;

    public StageInfo(int stageNumber, List<NodeData> nodes, float visitedNodesPercentage)
    {
        this.stageNumber = stageNumber;
        this.nodes = nodes;
        this.visitedNodesPercentage = visitedNodesPercentage;
    }
}

[System.Serializable]
public class NodeData
{
    public int nodeId;
    public NodeType nodeType;
    public bool visited;

    public NodeData(int nodeId, NodeType nodeType, bool visited)
    {
        this.nodeId = nodeId;
        this.nodeType = nodeType;
        this.visited = visited;
    }
}

[System.Serializable]
public class Wrapper<T>
{
    public List<T> items;

    public Wrapper(List<T> items)
    {
        this.items = items;
    }
}