using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Vector3 originalScale;

    void Start() {
        // Save the original scale
        originalScale = transform.localScale;
    }

    void Update () {
        Transform cameraTransform = Camera.main.transform;
        Vector3 cameraPos = cameraTransform.position;

        transform.LookAt(transform.position + cameraTransform.rotation * Vector3.forward,
                         cameraTransform.rotation * Vector3.up);

        // Check if the object is facing away from the camera
        Vector3 objectToCamera = cameraPos - transform.position;
        float angle = Vector3.Angle(transform.forward, objectToCamera);
        if (angle > 90) {
            // Flip the object if it's facing away from the camera
            transform.localScale = new Vector3(-originalScale.x, originalScale.y, originalScale.z);
        } else {
            // Otherwise, restore the original scale
            transform.localScale = originalScale;
        }
    }
}
