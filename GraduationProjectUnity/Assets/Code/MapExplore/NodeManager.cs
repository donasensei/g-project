using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public Node[] nodes;
    public Player playerPrefab;

    private Player playerInstance;
    private bool isMoving = false;

    [SerializeField] private int stageNumber = 0;

    private async void Start()
    {
        SaveManager.Initialize();
        StageInfo loadedStageInfo = LoadStageInfo(stageNumber);

        if (loadedStageInfo != null)
        {
            // Update the visited status of the nodes based on the loaded stage info
            for (int i = 0; i < nodes.Length; i++)
            {
                NodeData nodeData = loadedStageInfo.nodes.Find(data => data.nodeId == i);
                if (nodeData != null)
                {
                    nodes[i].visited = nodeData.visited;
                }
            }
        }
        else
        {
            Debug.LogWarning("No stage info found, starting a new game.");
        }

        StageInfo currentStageInfo = GenerateStageInfo(1);

        playerInstance = Instantiate(playerPrefab, nodes[0].transform.position, Quaternion.identity);
        UIManager.Instance.UpdateSearchProgress(PrintVisitedNodesPercentage());

        // Example: Move the player to a specific node and trigger the node event
        await playerInstance.MoveToNode(nodes[0]);
        TriggerNodeEvent(nodes[0]);
        ActivateConnectedNodes();
    }

    public List<NodeData> GenerateNodeDataList()
    {
        List<NodeData> nodeDataList = new List<NodeData>();

        for (int i = 0; i < nodes.Length; i++)
        {
            Node node = nodes[i];
            NodeData nodeData = new NodeData(i, node.nodeType, node.visited);
            nodeDataList.Add(nodeData);
        }

        return nodeDataList;
    }

    public StageInfo GenerateStageInfo(int stageNumber)
    {
        List<NodeData> nodes = GenerateNodeDataList();
        float visitedNodesPercentage = PrintVisitedNodesPercentage();

        StageInfo stageInfo = new StageInfo(stageNumber, nodes, visitedNodesPercentage);
        return stageInfo;
    }


    private void Update()
    {
        if (!isMoving && Input.GetMouseButtonDown(0))
        {
            HandleUserInput();
        }
    }

    private async void HandleUserInput()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Node targetNode = hit.collider.GetComponent<Node>();
            if (targetNode != null && System.Array.IndexOf(playerInstance.currentNode.connectedNodes, targetNode) >= 0)
            {
                isMoving = true;
                await MoveToNodeOnClick(targetNode);
                CheckForSearchEnd();
                ActivateConnectedNodes();
                isMoving = false;
            }
        }
    }

    public async Task MoveToNodeOnClick(Node targetNode)
    {
        await playerInstance.MoveToNode(targetNode);
        TriggerNodeEvent(targetNode);
        UIManager.Instance.UpdateSearchProgress(PrintVisitedNodesPercentage());
    }

    private void TriggerNodeEvent(Node targetNode)
    {
        targetNode.visited = true;
        EventManager.Instance.NodeEvent(targetNode.nodeType);
    }

    private void CheckForSearchEnd()
    {
        if (playerInstance.currentNode.connectedNodes.Length == 0)
        {
            UIManager.Instance.ShowSearchEnd();
            SaveStageInfo(); // Save the stage info when the search ends
        }
    }

    private void ActivateConnectedNodes()
    {
        // Deactivate all nodes that have not been visited
        foreach (Node node in nodes)
        {
            if (!node.visited)
            {
                node.gameObject.SetActive(false);
            }
        }

        // Activate only the nodes connected to the player's current node
        foreach (Node connectedNode in playerInstance.currentNode.connectedNodes)
        {
            connectedNode.gameObject.SetActive(true);
        }

        // Ensure the player's current node is active
        playerInstance.currentNode.gameObject.SetActive(true);
    }

    public float PrintVisitedNodesPercentage()
    {
        int visitedNodesCount = 0;

        foreach (Node node in nodes)
        {
            if (node.visited)
            {
                visitedNodesCount++;
            }
        }

        float visitedPercentage = (float)visitedNodesCount / nodes.Length * 100;
        Debug.LogFormat("Visited nodes percentage: {0}%", visitedPercentage);

        return visitedPercentage;
    }

    public void SaveStageInfo()
    {
        StageInfo currentStageInfo = GenerateStageInfo(stageNumber); // Replace with the desired stage number
        // List<StageInfo> stageInfoList = new List<StageInfo> { currentStageInfo };
        SaveManager.Instance.SaveStageInfo(currentStageInfo);
    }

    public StageInfo LoadStageInfo(int stageNumber)
    {
        List<StageInfo> stageInfoList = SaveManager.Instance.LoadStageInfo();
        StageInfo loadedStageInfo = stageInfoList.Find(stageInfo => stageInfo.stageNumber == stageNumber);

        if (loadedStageInfo != null)
        {
            Debug.Log("Stage info loaded.");
            return loadedStageInfo;
        }
        else
        {
            Debug.LogError("No stage info found for stage number " + stageNumber);
            return null;
        }
    }
}
