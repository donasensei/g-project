using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TreeEditor.TreeEditorHelper;

public class EventManager : MonoBehaviour
{
    public static EventManager Instance;

    private void Awake() // Singleton
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void NodeEvent(NodeType nodeType)
    {
        switch (nodeType)
        {
            case NodeType.None:
                Debug.Log("None");
                break;
            case NodeType.Battle:
                Debug.Log("Battle triggered");
                break;
            case NodeType.GetResource:
                Debug.Log("Get Random Resources");
                break;
            default:
                Debug.LogWarning("Unknown node type: " +  nodeType);
                break;
        }
        UIManager.Instance.UpdateEventText(nodeType.ToString());
    }
}
