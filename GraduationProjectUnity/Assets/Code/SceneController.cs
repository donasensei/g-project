using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    // 싱글톤 인스턴스
    private static SceneController _instance;
    // 외부에서 접근할 수 있는 싱글톤 인스턴스
    public static SceneController Instance { get { return _instance; } }

    // 초기화
    private void Awake()
    {
        #region 싱글톤
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            // 다른 씬으로 넘어가도 파괴되지 않도록 설정
            DontDestroyOnLoad(this.gameObject);
        }
        #endregion
    }

    // 모든 게임 오브젝트와 자원 제거
    private void DestroyAllObjectsAndResources()
    {
        // 모든 게임 오브젝트와 자원 제거 코드
        // 모든 게임 오브젝트를 찾아서 배열에 저장
        GameObject[] allObjects = FindObjectsOfType<GameObject>();

        // 배열의 모든 게임 오브젝트를 제거
        for (int i = 0; i < allObjects.Length; i++)
        {
            Destroy(allObjects[i]);
        }

        // 사용한 모든 리소스 해제
        Resources.UnloadUnusedAssets();
    }

    // Scene을 로드하는 메소드
    // 사용 예시: SceneController.Instance.LoadScene("Scene 이름")
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    // 게임 종료 (항상 이것으로 종료할 것)
    public void QuitGame()
    {
        OnApplicationQuit();
        // 게임 종료
        Application.Quit();
    }

    // 게임 종료 시 실행
    private void OnApplicationQuit()
    {
        // 게임 종료 시 실행할 코드
        DestroyAllObjectsAndResources();
    }
}