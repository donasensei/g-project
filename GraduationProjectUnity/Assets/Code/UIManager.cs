using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public Text searchEndText;
    public Text searchProgressText;
    public Text eventText;

    private void Awake() // Singleton
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ShowSearchEnd()
    {
        searchEndText.gameObject.SetActive(true);
    }

    public void HideSearchEnd()
    {
        searchEndText.gameObject.SetActive(false);
    }

    public void UpdateSearchProgress(float percentage)
    {
        searchProgressText.text = $"Progress :  {percentage}%";
    }

    public void UpdateEventText(string text)
    {
        eventText.text = $"Current Event : {text}";
    }

}
