using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CharacterSelector : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Transform targetPosition;
    [SerializeField] private float moveDuration = 0.5f;
    private Vector3 originalPosition;

    private void Start()
    {
        originalPosition = transform.position;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            SelectCharacter();
        }
    }

    public void SelectCharacter()
    {
        transform.DOMove(targetPosition.position, moveDuration).SetEase(Ease.OutQuad);
    }

    public void DeselectCharacter()
    {
        transform.DOMove(originalPosition, moveDuration).SetEase(Ease.OutQuad);
    }
}
