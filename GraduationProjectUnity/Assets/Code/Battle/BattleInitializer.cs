using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleInitializer : MonoBehaviour
{
    [SerializeField] private CombatManager combatManager;
    [SerializeField] private PartyManager partyManager;
    [SerializeField] private CharacterDataList enemyDataList;

    private void Start()
    {
        InitializeBattle();
    }

    private void InitializeBattle()
    {
        // Get player party info from PartyManager
        List<Character> playerParty = partyManager.PartyMembers;

        // Initialize combat with player party and enemy data from ScriptableObject
        combatManager.InitializeCombat(playerParty, enemyDataList);

        combatManager.PrintPartyInfo();
    }
}
