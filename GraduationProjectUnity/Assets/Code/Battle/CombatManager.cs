using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public float turnDuration = 10.0f;
    private List<Character> playerParty;
    private List<Character> enemyParty;
    private bool isPlayerTurn = true;

    // Character's position
    [SerializeField] private Character characterPrefab;

    [SerializeField] private List<GameObject> playerPositions;
    [SerializeField] private List<GameObject> enemyPositions;

    // Selecte Character
    [SerializeField] private int selectedCharacterIndex = 0;
    [SerializeField] private CharacterSelector[] characterSelectors;

    public TurnState CurrentTurnState { get; private set; }

    private void Start()
    {
        characterSelectors = new CharacterSelector[playerPositions.Count];
        for (int i = 0; i < playerPositions.Count; i++)
        {
            characterSelectors[i] = playerPositions[i].GetComponent<CharacterSelector>();
        }
    }

    private void Update()
    {
        if (CurrentTurnState == TurnState.PlayerPartyAction)
        {
            HandleKeyboardInput();
        }
    }

    // Set up the initial state of combat
    public async void InitializeCombat(List<Character> playerParty, CharacterDataList enemyDataList)
    {
        this.playerParty = playerParty; // Assign the passed playerParty to the CombatManager's playerParty

        enemyParty = new List<Character>();
        foreach (Character characterData in enemyDataList.characterDataList)
        {
            enemyParty.Add(characterData);
        }
        SpawnCharacters();


        CurrentTurnState = TurnState.PlayerPartyAction;
        await CombatLoop(CurrentTurnState);
    }

    public void PrintPartyInfo()
    {
        Debug.Log("Player Party:");
        foreach (Character character in playerParty)
        {
            Debug.Log(character.CharacterName);
        }

        Debug.Log("Enemy Party:");
        foreach (Character character in enemyParty)
        {
            Debug.Log(character.CharacterName);
        }
    }

    private void HandleKeyboardInput()
    {
        int previousCharacterIndex = selectedCharacterIndex;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectedCharacterIndex--;
            if (selectedCharacterIndex < 0)
            {
                selectedCharacterIndex = playerPositions.Count - 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectedCharacterIndex++;
            if (selectedCharacterIndex >= playerPositions.Count)
            {
                selectedCharacterIndex = 0;
            }
        }

        if (previousCharacterIndex != selectedCharacterIndex)
        {
            characterSelectors[previousCharacterIndex].DeselectCharacter();
            characterSelectors[selectedCharacterIndex].SelectCharacter();
        }
    }

    private void SpawnCharacters()
    {
        for (int i = 0; i < playerParty.Count; i++)
        {
            SpriteRenderer playerSpriteRenderer = playerPositions[i].GetComponent<SpriteRenderer>();
            playerSpriteRenderer.sprite = playerParty[i].CharacterSprite;
            playerPositions[i].SetActive(true);
        }

        for (int i = 0; i < enemyParty.Count; i++)
        {
            SpriteRenderer enemySpriteRenderer = enemyPositions[i].GetComponent<SpriteRenderer>();
            enemySpriteRenderer.sprite = enemyParty[i].CharacterSprite;
            enemyPositions[i].SetActive(true); 
        }
    }

    // The main combat loop
    private async Task CombatLoop(TurnState currentTurnState)
    {
        switch (currentTurnState)
        {
            case TurnState.PlayerPartyAction:
                // Decide player party actions
                await CombatLoop(TurnState.EnemyPartyAction);
                break;

            case TurnState.EnemyPartyAction:
                // Decide enemy character actions (AI)
                await CombatLoop(TurnState.ExecuteActions);
                break;

            case TurnState.ExecuteActions:
                // Execute actions
                await CombatLoop(TurnState.CheckIncapacitated);
                break;

            case TurnState.CheckIncapacitated:
                // Check incapacitated characters
                await CombatLoop(TurnState.CheckWinLoss);
                break;

            case TurnState.CheckWinLoss:
                // Check player win/loss
                // If win or loss, update currentTurnState to EndCombat
                break;

            case TurnState.EndCombat:
                // End combat
                return; // Exit the recursion
        }
        Debug.Log(CurrentTurnState);
        await Task.Delay(1000); // Wait for a second before moving to the next state
    }


    private void SetupCombat()
    {
        // Implement any necessary setup logic for combat
    }

    private async Task DecidePlayerActions()
    {
        // Implement player action decision logic, e.g. through a user interface
    }

    private void DecideEnemyActions()
    {
        // Implement enemy action decision logic (AI)
    }

    private async Task ExecuteActions()
    {
        // Implement asynchronous action execution logic
    }

    private void CheckIncapacitatedCharacters()
    {
        // Implement incapacitated character check logic
    }

    private bool CheckWinLossConditions()
    {
        // Implement win/loss condition check logic
        // Return true if a win/loss condition has been met, otherwise return false
        return false;
    }
}

public enum TurnState
{
    SetUp,
    PlayerPartyAction,
    EnemyPartyAction,
    ExecuteActions,
    CheckIncapacitated,
    CheckWinLoss,
    EndCombat
}